<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sdkretek' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'gU1p(IKL?A^}%XM &Tnd!Q8LDZ82g:[&=&#8AEC`Kc|:?-Hx^5wBY4$4|Hqe g.!' );
define( 'SECURE_AUTH_KEY',  'L4~=_fm(jo-e!|K77y7s+CrO/=HJJchXq:tpl@<49Hr=f9!*`#lIOKnX`lH w8g-' );
define( 'LOGGED_IN_KEY',    '@8,P|,lN-EyLfza92x>:6cGfB-KPb:7OktR)! %Au [>88<@oltL^QzlwP&6}NF:' );
define( 'NONCE_KEY',        'jo+$q?IUs;Mt_h$N6%+ B*~.vkX!owAz><qw% K~</ 7%o1Nb!MwoZ:R2lxYzJa,' );
define( 'AUTH_SALT',        'I:RCefmT`E#0w^o#Xx)X{=3Q(3%e-1rqY`d9p{Pg9z; CL#dF05-!NmBiZc8!Kct' );
define( 'SECURE_AUTH_SALT', 'ft)O26.dc&,j>4v6tgXtsn76DTra_Kv{UB,#J&l,`m#sFPzSuV,v*AsK1,BXUOQl' );
define( 'LOGGED_IN_SALT',   '^;#;9(hDF{7oyw7Dli2J li9sbL9wOyw43-4b +qmOk`%zCfcX[.V~d:ecFqQq!r' );
define( 'NONCE_SALT',       'g+OtAckW(_-_IxI]aVqyOq{x*lQ$k`Q76 26_<f::^af2KVWI>_ro_btcP!I,Sb?' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define('WP_ALLOW_REPAIR', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
